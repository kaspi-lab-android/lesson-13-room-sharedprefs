package com.example.room.data

import androidx.room.Embedded
import androidx.room.Relation

// https://developer.android.com/training/data-storage/room/relationships

data class PersonWithPets(
    @Embedded
    val person: Person,

    @Relation(parentColumn = Person.COLUMN_PERSON_ID, entityColumn = Pet.COLUMN_OWNER_ID)
    val petList: List<Pet> = emptyList()
)

