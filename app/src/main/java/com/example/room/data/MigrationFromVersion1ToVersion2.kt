package com.example.room.data

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

class MigrationFromVersion1ToVersion2 : Migration(1, 2){
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE person ADD COLUMN age INTEGER")
    }
}