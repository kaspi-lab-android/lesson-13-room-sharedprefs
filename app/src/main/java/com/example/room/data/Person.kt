package com.example.room.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.room.data.Person.Companion.TABLE_NAME
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = TABLE_NAME)
data class Person(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_PERSON_ID)
    val personId: Int,
    @ColumnInfo(name = COLUMN_FIRST_NAME)
    val firstName: String?,
    @ColumnInfo(name = COLUMN_LAST_NAME)
    val lastName: String? = null,
    @ColumnInfo(name = COLUMN_CREATED_DATE)
    val createdDate: Calendar = Calendar.getInstance()
//    @ColumnInfo(name = COLUMN_AGE)
//    val age: Int
) {

    override fun toString(): String =
        """
            Person(
                id=$personId,
                firstName=$firstName,
                lastName=$lastName,
                createdDate=${SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(createdDate.time)}
            )
        """.trimIndent()

    companion object {
        const val TABLE_NAME = "persons"
        const val COLUMN_PERSON_ID = "person_id"
        const val COLUMN_FIRST_NAME = "first_name"
        const val COLUMN_LAST_NAME = "last_name"
        const val COLUMN_CREATED_DATE = "created_date"
        const val COLUMN_AGE = "age"
    }
}