package com.example.room.data

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.example.room.data.Person.Companion.TABLE_NAME

@Dao
interface PersonWithPetsDao {

    /**
     * Этот запрос сообщит Room о необходимости запроса таблиц [Person] и [Pet]
     * и обработки сопоставления объектов.
     */
    @Transaction
    @Query("SELECT * from $TABLE_NAME")
    fun getPersonsWithPets(): List<PersonWithPets>
}