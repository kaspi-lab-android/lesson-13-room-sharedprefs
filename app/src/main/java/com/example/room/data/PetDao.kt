package com.example.room.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import com.example.room.data.Pet.Companion.TABLE_NAME

@Dao
interface PetDao {
    @Query("SELECT * FROM $TABLE_NAME")
    fun getAll(): List<Pet>

    @Delete
    fun delete(pet: Pet)
}