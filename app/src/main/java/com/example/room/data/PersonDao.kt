package com.example.room.data

import androidx.room.*
import com.example.room.data.Person.Companion.COLUMN_FIRST_NAME
import com.example.room.data.Person.Companion.COLUMN_LAST_NAME
import com.example.room.data.Person.Companion.COLUMN_PERSON_ID
import com.example.room.data.Person.Companion.TABLE_NAME

@Dao
interface PersonDao {
    @Query("SELECT * FROM $TABLE_NAME")
    fun getAll(): List<Person>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<Person>)

    @Delete
    fun delete(user: Person)

    @Query("SELECT * FROM $TABLE_NAME WHERE $COLUMN_PERSON_ID IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<Person>

    @Query("""SELECT * FROM $TABLE_NAME WHERE $COLUMN_FIRST_NAME LIKE :first 
            AND $COLUMN_LAST_NAME LIKE :last LIMIT 1""")
    fun findByName(first: String, last: String): Person
}