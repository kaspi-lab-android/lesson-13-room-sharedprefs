package com.example.room.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.example.room.data.Person.Companion.COLUMN_PERSON_ID
import com.example.room.data.Pet.Companion.COLUMN_OWNER_ID
import com.example.room.data.Pet.Companion.TABLE_NAME

@Entity(
    tableName = TABLE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = Person::class,
            parentColumns = [COLUMN_PERSON_ID],
            childColumns = [COLUMN_OWNER_ID],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class Pet(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_PET_ID)
    val petId: Int,
    @ColumnInfo(name = COLUMN_OWNER_ID)
    val ownerId: Int,
    @ColumnInfo(name = COLUMN_PET_NAME)
    val petName: String
) {

    companion object {
        const val TABLE_NAME = "pets"
        const val COLUMN_PET_ID = "pet_id"
        const val COLUMN_OWNER_ID = "owner_id"
        const val COLUMN_PET_NAME = "name"
    }
}