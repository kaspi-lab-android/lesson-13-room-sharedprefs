package com.example.room

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.example.room.data.Person
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private var executors = Executors()
    private var preferences: SharedPreferences? = null
    private var listener: SharedPreferences.OnSharedPreferenceChangeListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbButton.setOnClickListener {
            insertData()
        }

        listener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
            showData(key.toString() + ": " + sharedPreferences.getString(key, ""))
        }
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        preferences?.registerOnSharedPreferenceChangeListener(listener)

        prefsButton.setOnClickListener {
            preferences?.edit()
                ?.putString(PREF_KEY_NAME, "Zhanbolat Raimbekov")
                ?.apply()
        }
    }

    override fun onDestroy() {
        preferences?.unregisterOnSharedPreferenceChangeListener(listener)
        executors.shutdown()
        super.onDestroy()
    }

    private fun insertData() {
        executors.diskIO().execute {
            val insertList = listOf(
                Person(
                    personId = 1,
                    firstName = "Zhanbolat",
                    lastName = "Raimbekov",
                    createdDate = Calendar.getInstance()
                ),
                Person(
                    personId = 2,
                    firstName = "Azat"
                ),
                Person(
                    personId = 3,
                    firstName = "John",
                    lastName = "Doe"
                )
            )

            (applicationContext as App).db.personDao()
                .insertAll(insertList)

            val dbList = (applicationContext as App).db.personDao().getAll()

            val string = StringBuilder()
            string.append("${dbList.size} inserted.\n\n")
            dbList.forEach {
                string.append(it.toString()).append("\n")
            }

            executors.mainThread().execute {
                showData(string.toString())
            }
        }
    }

    private fun showData(string: String) {
        dbTextView.text = string
    }

    companion object {
        const val PREF_KEY_NAME = "KEY_NAME"
    }
}
